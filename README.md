## OpenStack SDK install
This README will cover installing and configuring the OpenStack SDK in detail on both Linux and MacOs systems and most of the steps detailed in this document come from OpenStack's official documentation found at https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html which also contans details on how to install it on Windows systems as-well.

### Steps
1. Install pip onto the machine used. On MacOS this can be done using the following command "easy_install pip" and on Ubuntu or Debian based systems it is an apt package install with "apt install python3-pip"
2. Next with pip installed use that to install the packages needed using the requirements file.
To do this you need to use the following command `pip install -r requirements.txt` and have the requirements file in the directory you are running this command or replace the name of the file with the path to it if it is housed in a different directory.
If when installing on MacOS with a pip install you get an error that reads "ERROR: Failed building wheel for netifaces" this is caused by an outdated version of XCode command line tools. To fix this problem uninstall the command line tools using this guide https://mac.install.guide/commandlinetools/6 then to reinstall run this command "xcode-select --install" which will open a dialog box on the desktop screen asking for confirmation to install these tools. Once the tools are installed run "xcode-select -p" to see if the xcode folder exists, if so they are installed. With this done try rerunning the command to install the client or try installing netifaces itself using pip and this should resolve the issue.
If installing this on Fedora 40 development dependencies required to be installed such as:
**sudo dnf groupinstall "Development Tools"**
**sudo dnf install python3-devel openssl-devel zlib-devel bzip2-devel sqlite-devel libffi-devel**

4. Before procceding with the next steps check if the openstack is installed using the following command "openstack --version". This should display the version number which the latest as of the time of writing is the "openstack 6.5.0".
5. To run commands on the openstack client on the cloud it needs to have the right credentials so it can verify the user. In the top right corner of the OpenStack horizon dashboard click the dropdown and they click the download for the OpenStack RC File which will download "admin-openrc.sh" or the equivalent rc file for the relevant user. Source this file in the same directory you want to use the client in with source [file/path/to/file] this will prompt you to enter a password before the file is sourced which will be your relevant users login password.By entering in the correct password it will authorise you to use the client as that user.
6. If you try running the client now it will hang for a while until producing the following exceptions when failing requests.exceptions.ConnectTimeout: HTTPConnectionPool(host='cloud.waltoninstitute.ie', port=5000): Max retries exceeded with url: /v3/auth/tokens (Caused by ConnectTimeoutError(<urllib3.connection.HTTPConnection object at 0x108df5890>, 'Connection to cloud.waltoninstitute.ie timed out. (connect timeout=None)'))" and "keystoneauth1.exceptions.connection.ConnectTimeout: Request to http://cloud.waltoninstitute.ie:5000/v3/auth/tokens timed out". These exceptions are due to the client not being able to resolve the url of the controller as there has not been a route set up for it in the hosts file for a DNS entry. This is why it says it is something like "Failed to discover available identity versions when contacting http://cloud.waltoninstitute.ie:5000/v3/. Attempting to parse version from URL.
Request to http://cloud.waltoninstitute.ie:5000/v3/auth/tokens timed out". To resolve this issue we need to add an entry to the hosts file to pint to the right ip for in this case pointing to cloud.waltoninstitute.ie. The entry in the hosts file '/etc/hosts' would be formatted like this.
10.37.93.100    controller   cloud.waltoninstitute.ie.
In this case the DNS entry is pointing to the OpenStack controller. This is because the client has the ability to perform CRUD operations on the OpenStack cloud therefore needs access to the controller to perform these operations even if a user does not have permissions to utilise all functions.
6. With this it should be possible to contact the server and pull data of of it. Using the command "openstack image list" will list all images for all tenants. To list all volumes for all tenants the option '--all' needs to be added to the command "openstack volume list" as the command on its own only lists the volumes for the logged in tenant which in the case of "admin" their is no volumes.
### Tips for Python script
### Disclaimer:
Most of the scripts created created in this directory were created with the 'admin' user utilised and as such the authentication for this user is different to setup comared to more standard users. The only other user that was used when testing these scripts was 'cloud2021_admin' for the 'openstack_add_metadata_to_vms_via_key.py' script as only people in a group can add or edit metadata of VMs that belong to that group. This means that usage of these scripts may vary depending on the openstack user. For example if a user that only belonged to the INFRASTRUCTURE group tried to use the metadata_check.py script, they would have to first change the authentication to match a user account and remove the search opts as they would not apply to a single user. Then they would then theoretically only be able to view the servers that are a part of that group and only test them for  metadata.

Since the update from keystoneclient.v2 to v3 it is needed to set up an authorisation session using the  variables from the "openrc" file that was sourced to use the SDK. To set up an authorisation session both "session" and "v3" need to be imported from keystoneauth1.identity. To setup a "Password" it needs to have the following format.
```
auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           project_name=env['OS_PROJECT_NAME'],
                           user_domain_name=env['OS_USER_DOMAIN_NAME'],
                           project_domain_name=env['OS_PROJECT_DOMAIN_ID'])
```
All the variables here are taken from the openrc file and are imported with environ using the following import. "from os import environ as env". With a "Password" set up you can now create a session to validate the client so you can save the client instance so you do not have to re-authenticate after every cal to the client. To create a session is as follows.
```
sess = session.Session(auth=auth)
```
Now to create a client instance you need to add the session and for some clients such as nova client the version of the client you want to use. This is done like so.
```
nova = novaclient.Client(version='2.0',session=sess)
```
If you try to list all images now using the nova.server.list() inbuilt function you will only get a list of the servers that are specific to the tenant of that user, in the case of the 'admin' user this displays none. To list all server for all tenants a search option needs to be added like so.
```
search_opts = {'all_tenants': 1}
```
The option option 'all_tenants' is for selecting all tenants that exist on the OpenStack cloud instance. Setting this to 1 is to get add all tenants to the search options. When listing the servers with a the "nova.servers.list()" command add the "search_opts" or search options to the command like so.
```
servers_list = nova.servers.list(search_opts=search_opts)
```
This line is saving the output of that command to "servers_list" which then. can be iterated thought with a for loop to get each individual server.
Each server is essentially its own directory in python. Converting a server to a directory and printing it will shown the keys associated with a server such as id, name, security_groups, key_name, metadata, etc.

Below is a completed script that scans through the OpenStack servers list and lists various attributes for each server to various files depending on conditions such as state and absence of certain fields in the metadata.
```
import os
import subprocess
from os import environ as env
from novaclient import client as novaclient
import glanceclient.v2.client as glclient
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client

# Opening all relevant files to later write to them
all = open('all_instances.txt' , 'w')
nokeys = open('no_keys.txt', 'w')
shutoff = open ('shutoff_instances.txt' , 'w')
error = open ('error_instances.txt' , 'w')
paused = open ('paused_instances.txt' , 'w')
test = open ('test_instances.txt' , 'w')
prod = open ('prod_instances.txt' , 'w')
missing_metadata = open ('missing_metadata.txt' , 'w')
valid_metadata = open ('valid_metadata.txt' , 'w')

# Helper function to write data to files
def print_to_file(serverId,filename):
    print("Server name & id: " + server.id + " " + server.name, file = filename)
    print("Server status: " + server.status, file = filename)
    print("Server creation date: " + server.created, file = filename)
    print("Host Id: " + server.hostId, file = filename)
    print("User Id: " + server.user_id, file = filename)
    print("Flavor: " + str(server.flavor), file = filename)
    try:
        print("Security Groups: " + str(server.security_groups), file = filename)
    except:
        print("Security Groups: " + "None", file = filename)
    print("Key Name: " + str(server.key_name), file = filename)
    print("Metadata: " + str(server.metadata), file = filename)
    # volumes = subprocess.run(["openstack server show " + serverId + " | grep volumes_attached -A 1", "/dev/null"],shell=True, capture_output=True, text=True)
    # print(volumes.stdout, file = filename)
    print("", file = filename)

# Setting up auth to later create a session
auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           project_name=env['OS_PROJECT_NAME'],
                           user_domain_name=env['OS_USER_DOMAIN_NAME'],
                           project_domain_name=env['OS_PROJECT_DOMAIN_ID'])

sess = session.Session(auth=auth)
# Created session is used to spawn a nova-client instance to access compute details
nova = novaclient.Client(version='2.0',session=sess)
# Search opts to add all tenants to server list
search_opts = {'all_tenants': 1}
flavors_list = nova.flavors.list()
# Searching list of servers, if they fall into one of theses states or have no key,
# their information is written to the relevant file.
servers_list = nova.servers.list(search_opts=search_opts)
for server in servers_list:
    # All instances that came up in list regardless of state/lack of key
    print_to_file(server.id,all)
    # Instances without a SSH key assigned to them
    if (str(server.key_name) == "None"):
        print_to_file(server.id,nokeys)
    # Instances that are shutoff
    if (server.status == "SHUTOFF"):
        print_to_file(server.id,shutoff)
    # Instanes in an error state
    if (server.status == "ERROR"):
        print_to_file(server.id,error)
    # Instances that are paused for some reason
    if (server.status == "PAUSED"):
        print_to_file(server.id,paused)
    # Check if server name has test in it
    if 'test' in server.name:
        print_to_file(server.id,test)
    # Checking for known production servers
    if 'prod' in server.name:
        print_to_file(server.id,prod)
    # Checking if all three metadata fields don't exist. If so push server Id & name to missing_metadata.txt else write to valid_metadata.txt
    if 'Maintainer' not in str(server.metadata) and 'Keyholder' not in str(server.metadata) and 'Unit' not in str(server.metadata):
        print(server.id + " " + server.name , file = missing_metadata)
    else:
        print(server.id + " " + server.name, file = valid_metadata)

```
