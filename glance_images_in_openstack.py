import csv
import os
import subprocess
import os.path
from os import environ as env
from novaclient import client as novaclient
import glanceclient.v2.client as glclient
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client

if os.path.isfile("glance_images.csv"):

    exists = open('glance_exists_in_openstack.txt' , 'w')
    missing = open('glance_missing_from_openstack.txt' , 'w')

    with open("glance_images.csv", 'r') as file:
        csvreader = csv.reader(file)

        auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                                   username=env['OS_USERNAME'],
                                   password=env['OS_PASSWORD'],
                                   project_name=env['OS_PROJECT_NAME'],
                                   user_domain_name=env['OS_USER_DOMAIN_NAME'],
                                   project_domain_name=env['OS_PROJECT_DOMAIN_ID'])

        sess = session.Session(auth=auth)
        # Created session is used to spawn a nova-client instance to access compute details
        nova = novaclient.Client(version='2.0',session=sess)
        glance = glclient.Client('2',session=sess)
        # Search opts to add all tenants to server list
        search_opts = {'all_tenants': 1}
        for row in csvreader:
            try:
                glance.images.get(row[0])
                print(row[0] + " " + glance.images.get(row[0]).name , file = exists)
            except Exception as e:
                print(row[0] , file = missing)

        print("Two text files named 'glance_exists_in_openstack.txt' and 'glance_missing_from_openstack.txt' have been generated containing the results" + "\n" +
        "of this script")
else:
    print("File containing glance images 'glance_images.csv' is missing. Please create file of this name with glance images ids to test" + '\n' +
    "to continue")
