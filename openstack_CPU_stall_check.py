import os
import subprocess
from datetime import date
from os import environ as env
from novaclient import client as novaclient
import glanceclient.v2.client as glclient
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client
import smtplib
from email.mime.text import MIMEText

# Setting up auth to later create a session
auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           project_name=env['OS_PROJECT_NAME'],
                           user_domain_name=env['OS_USER_DOMAIN_NAME'],
                           project_domain_name=env['OS_PROJECT_DOMAIN_ID'])

sess = session.Session(auth=auth)
# Created session is used to spawn a nova-client instance to access compute details
nova = novaclient.Client(version='2.0',session=sess)
# Search opts to add all tenants to server list
search_opts = {'all_tenants': 1}
# Searching list of servers, if they fall into one of theses states or have no key,
# their information is written to the relevant file.
servers_list = nova.servers.find(search_opts=search_opts)
for server in servers_list:
    try:
        if "watchdog: BUG: soft lockup" in str(server.get_console_output(10)):
            server_details = server.id + " " + server.name
            console_output = server.get_console_output(10)
            msg = MIMEText(str(server_details + "/n" + console_output))
            print(msg.as_string())
    except Exception as e:
        print("No console avalable. Instance is probably shut off.")
        print("----------------------------")
