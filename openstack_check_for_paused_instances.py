# This script uses the 'admin' user credentials to check all
# VMs on OpenStack if the 'Power state' of the VM is paused.
# This is different then checking if the status of the Vm is paused
# as the VM status does not change unless it is sent a request to do so.
# In this case the VM is pausing on its own due to primarly CPU stalls
# To check if the VM is in this state the VMs power state needs to be checked
# which is what this cript does and prints the results to the console.
# If no results are printed that means no VMs are in this state.

import os
import subprocess
from datetime import date
from os import environ as env
from novaclient import client as novaclient
import glanceclient.v2.client as glclient
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client

# Setting up auth to later create a session
auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           project_name=env['OS_PROJECT_NAME'],
                           user_domain_name=env['OS_USER_DOMAIN_NAME'],
                           project_domain_name=env['OS_PROJECT_DOMAIN_ID'])

sess = session.Session(auth=auth)
# Created session is used to spawn a nova-client instance to access compute details
nova = novaclient.Client(version='2.0',session=sess)
# Search opts to add all tenants to server list
search_opts = {'all_tenants': 1}
# Searching list of servers, if they fall into one of theses states or have no key,
# their information is written to the relevant file.
servers_list = nova.servers.list(search_opts=search_opts)
for server in servers_list:
    task_state = getattr(server, 'OS-EXT-STS:task_state')
    power_state = getattr(server,'OS-EXT-STS:power_state')
    vm_state = getattr(server,'OS-EXT-STS:vm_state')
    host = getattr(server,'OS-EXT-SRV-ATTR:host')
    if str(power_state) == "3" and str(vm_state) != 'paused':
        print(server.id + " " + server.name)
        # print("Key name: " + str(server.key_name) + " " +  '\n'  + str(server.networks))
        # print("Host: " + host)
        # print("Task state: " + str(task_state))
        # print("Power state: " + str(power_state))
        # print("VM_state: " + str(vm_state))
        # print("---------------------")
