# This script is an example of some of the various checks that can be done using the various
# openstack clients. These checks include things such as getting server details like host and user IDs,
# the falvour of a server (which as of the time of writing does not appear in the info section on the horizon dashboard)
# It is also possible to get the vms current state and to check the servers names for keywords such as in this case 'test' and
# 'prod'. Finally is a small example of how the metadata check works by checking for keys in the metadata field of a server object.
# More of these fields can be seen from the 'server_keys' list available in this repo.


import os
import subprocess
from os import environ as env
from novaclient import client as novaclient
import glanceclient.v2.client as glclient
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client

# Opening all relevant files to later write to them
all = open('all_instances.txt' , 'w')
nokeys = open('no_keys.txt', 'w')
shutoff = open ('shutoff_instances.txt' , 'w')
error = open ('error_instances.txt' , 'w')
paused = open ('paused_instances.txt' , 'w')
test = open ('test_instances.txt' , 'w')
prod = open ('prod_instances.txt' , 'w')
missing_metadata = open ('missing_metadata.txt' , 'w')
valid_metadata = open ('valid_metadata.txt' , 'w')

# Helper function to write data to files
def print_to_file(serverId,filename):
    print("Server name & id: " + server.id + " " + server.name, file = filename)
    print("Server status: " + server.status, file = filename)
    print("Server creation date: " + server.created, file = filename)
    print("Host Id: " + server.hostId, file = filename)
    print("User Id: " + server.user_id, file = filename)
    print("Flavor: " + str(server.flavor), file = filename)
    try:
        print("Security Groups: " + str(server.security_groups), file = filename)
    except:
        print("Security Groups: " + "None", file = filename)
    print("Key Name: " + str(server.key_name), file = filename)
    print("Metadata: " + str(server.metadata), file = filename)
    # volumes = subprocess.run(["openstack server show " + serverId + " | grep volumes_attached -A 1", "/dev/null"],shell=True, capture_output=True, text=True)
    # print(volumes.stdout, file = filename)
    print("", file = filename)

# Setting up auth to later create a session
auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           project_name=env['OS_PROJECT_NAME'],
                           user_domain_name=env['OS_USER_DOMAIN_NAME'],
                           project_domain_name=env['OS_PROJECT_DOMAIN_ID'])

sess = session.Session(auth=auth)
# Created session is used to spawn a nova-client instance to access compute details
nova = novaclient.Client(version='2.0',session=sess)
# Search opts to add all tenants to server list
search_opts = {'all_tenants': 1}
flavors_list = nova.flavors.list()
# Searching list of servers, if they fall into one of theses states or have no key,
# their information is written to the relevant file.
servers_list = nova.servers.list(search_opts=search_opts)
for server in servers_list:
    # All instances that came up in list regardless of state/lack of key
    print_to_file(server.id,all)
    # Instances without a SSH key assigned to them
    if (str(server.key_name) == "None"):
        print_to_file(server.id,nokeys)
    # Instances that are shutoff
    if (server.status == "SHUTOFF"):
        print_to_file(server.id,shutoff)
    # Instanes in an error state
    if (server.status == "ERROR"):
        print_to_file(server.id,error)
    # Instances that are paused for some reason
    if (server.status == "PAUSED"):
        print_to_file(server.id,paused)
    # Check if server name has test in it
    if 'test' in server.name:
        print_to_file(server.id,test)
    # Checking for known production servers
    if 'prod' in server.name:
        print_to_file(server.id,prod)
    # Checking if all three metadata fields don't exist. If so push server Id & name to missing_metadata.txt else write to valid_metadata.txt
    if 'Maintainer' not in str(server.metadata) and 'Keyholder' not in str(server.metadata) and 'Unit' not in str(server.metadata):
        print(server.id + " " + server.name , file = missing_metadata)
    else:
        print(server.id + " " + server.name, file = valid_metadata)
