# Using 'admin' user credentials all VMs are checked for
# if they contain certain metadata fields. Depending on
# if they pass or fail this check the results are written
# to the appropriate file.

import os
import subprocess
from datetime import date
from os import environ as env
from novaclient import client as novaclient
import glanceclient.v2.client as glclient
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client

# Opening all relevant files to later write to them
missing_metadata = open ('missing_metadata.txt' , 'w')
valid_metadata = open ('valid_metadata.txt' , 'w')

# Setting up auth to later create a session
auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           project_name=env['OS_PROJECT_NAME'],
                           user_domain_name=env['OS_USER_DOMAIN_NAME'],
                           project_domain_name=env['OS_PROJECT_DOMAIN_ID'])

sess = session.Session(auth=auth)
# Created session is used to spawn a nova-client instance to access compute details
nova = novaclient.Client(version='2.0',session=sess)
# Search opts to add all tenants to server list
search_opts = {'all_tenants': 1}
# Searching list of servers, if they fall into one of theses states or have no key,
# their information is written to the relevant file.
servers_list = nova.servers.list(search_opts=search_opts)
for server in servers_list:

    # Checking if all three metadata fields don't exist. If so push server Id & name to missing_metadata.txt else write to valid_metadata.txt
    if 'Maintainer' not in str(server.metadata) and 'Keyholder' not in str(server.metadata) and 'Unit' not in str(server.metadata) and 'Group' not in str(server.metadata) and 'Production' not in str(server.metadata):
        print(server.id + " " + server.name + " " +  '\n' + str(server.networks), file = missing_metadata)
        print("" , file = missing_metadata)
    else:
        print(server.id + " " + server.name + " " +  '\n' + str(server.networks), file = valid_metadata)
        print("" , file = valid_metadata)

print("Two text files named 'valid_metadata.txt' and 'missing_metadata.txt' have been generated containing the results" + "\n" +
"of this check")
