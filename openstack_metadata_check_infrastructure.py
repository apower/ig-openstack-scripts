# Despite the name of this script this can be ran using any user besides 'admin',
# and can get the list of VMs available to that user from the groups they are in.
# For example testing of this script was done using the 'cloud2021_admin' user,
# when they were in the INFRASTRUCTURE group hence the name. This is done via the
# openrc file you can download from the dashboard and will give you a openrc file
# with the group you were assigned to when you downloaded the file.

# This script is the similar to the other metadata script but can only check VMs
# that belong to a specific group. It gets a list of these VMs and then checks the metadata
# of them to see if they contain the necessary metadata fields and are then wrote to one of
# two files depending on the outcome.

import os
import subprocess
from datetime import date
from os import environ as env
from novaclient import client as novaclient
import glanceclient.v2.client as glclient
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client

# Opening all relevant files to later write to them
infrastructure_missing_metadata = open ('infrastructure_missing_metadata.txt' , 'w')
infrastructure_valid_metadata = open ('infrastructure_valid_metadata.txt' , 'w')

# Setting up auth to later create a session
auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           project_name=env['OS_PROJECT_NAME'],
                           user_domain_name=env['OS_USER_DOMAIN_NAME'],
                           project_domain_name=env['OS_PROJECT_DOMAIN_ID'],
                           project_id=env['OS_PROJECT_ID'])

sess = session.Session(auth=auth)
# Created session is used to spawn a nova-client instance to access compute details
nova = novaclient.Client(version='2.0',session=sess)
# Search opts to add all tenants to server list
search_opts = {'all_tenants': 1}

# Getting all servers that key name is the one specified
# Change key name to the one desired for search
server_list = nova.servers.findall()

print(server_list)
for server in server_list:
    print(server.networks)
    # try:
    #     if "watchdog: BUG: soft lockup" in str(server.get_console_output(10)):
    #         print(server.id + " " + server.name)
    #         print(server.get_console_output(10))
    #         print("----------------------------")
    # except Exception as e:
    #     print("No console avalable. Instance is probably shut off.")
    #     print("----------------------------")

    # Checking if all three metadata fields don't exist. If so push server Id & name to missing_metadata.txt else write to valid_metadata.txt
    if 'Maintainer' not in str(server.metadata) and 'Keyholder' not in str(server.metadata) and 'Unit' not in str(server.metadata) and 'Group' not in str(server.metadata) and 'Production' not in str(server.metadata):
        print(server.id + " " + server.name + " " +  '\n' + str(server.networks), file = infrastructure_missing_metadata)
        print("" , file = infrastructure_missing_metadata)
    else:
        print(server.id + " " + server.name + " " +  '\n' + str(server.networks), file = infrastructure_valid_metadata)
        print("" , file = infrastructure_valid_metadata)