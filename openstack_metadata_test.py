# Use with group open-rc file
import os
import subprocess
from datetime import date
from os import environ as env
from novaclient import client as novaclient
import glanceclient.v2.client as glclient
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client


# Setting up auth to later create a session
auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           project_name=env['OS_PROJECT_NAME'],
                           user_domain_name=env['OS_USER_DOMAIN_NAME'],
                           project_domain_name=env['OS_PROJECT_DOMAIN_ID'],
                           project_id=env['OS_PROJECT_ID'])

sess = session.Session(auth=auth)
# Created session is used to spawn a nova-client instance to access compute details
nova = novaclient.Client(version='2.0',session=sess)
# Search opts to add all tenants to server list
search_opts = {'all_tenants': 1}
# Searching list of servers, if they fall into one of theses states or have no key,
# their information is written to the relevant file.
server_list = nova.servers.findall(key_name='jronan_rsa_work')
metadata = {
  "Maintainer": "John Ronan",
  "Keyholder": "John Ronan",
  "Unit": "RITS",
  "Group": "Infrastructure",
  "Production": "Yes"
}
print(server_list)
for server in server_list:
    if server.status != "ERROR":
        nova.servers.set_meta(server,metadata)
