# This script checks all VMs using 'admin' user credentials,
# and gathers their security groups in a dictionary.
# The name value for the key 'name' in this dictionary is checked,
# with the desired security group name (in this case being 
# 'ssh open world') and if that name matches the details
# of the VM is printed to the console. 

import os
import subprocess
from datetime import date
from os import environ as env
from novaclient import client as novaclient
import glanceclient.v2.client as glclient
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client

# Setting up auth to later create a session
auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           project_name=env['OS_PROJECT_NAME'],
                           user_domain_name=env['OS_USER_DOMAIN_NAME'],
                           project_domain_name=env['OS_PROJECT_DOMAIN_ID'])

sess = session.Session(auth=auth)
# Created session is used to spawn a nova-client instance to access compute details
nova = novaclient.Client(version='2.0',session=sess)
# Search opts to add all tenants to server list
search_opts = {'all_tenants': 1}

servers_list = nova.servers.list(search_opts=search_opts)
for server in servers_list:
    security_groups = server.list_security_group()
    for security_group in security_groups:
        if security_group.name == 'ssh open world' or security_group.name == 'wide-open-all':
            print(server.id + " " + server.name)
            # print(security_groups)
    