import csv
import os
import subprocess
from os import environ as env
from cinderclient.v3 import client as cinderclient
import openstack
import glanceclient.v2.client as glclient
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client

if os.path.isfile("quobyte_volumes.csv"):

    exists = open('quobyte_exists_in_openstack.txt' , 'w')
    missing = open('quobyte_missing_from_openstack.txt' , 'w')

    with open("quobyte_volumes.csv", 'r') as file:
        csvreader = csv.reader(file)

        auth = v3.Password(auth_url=env['OS_AUTH_URL'],
                                   username=env['OS_USERNAME'],
                                   password=env['OS_PASSWORD'],
                                   project_name=env['OS_PROJECT_NAME'],
                                   user_domain_name=env['OS_USER_DOMAIN_NAME'],
                                   project_domain_name=env['OS_PROJECT_DOMAIN_ID'])

        sess = session.Session(auth=auth)
        # Created session is used to spawn a nova-client instance to access compute details
        cinder = cinderclient.Client(session=sess)
        glance = glclient.Client('2',session=sess)
        # Search opts to add all tenants to server list
        search_opts = {'all_tenants': 1}
        for row in csvreader:
            try:
                print(row[0] + " " + cinder.volumes.get(row[0]).name , file = exists)
            except Exception as e:
                print(row[0] , file = missing)


        print("Two text files named 'quobyte_exists_in_openstack.txt' and 'quobyte_missing_from_openstack.txt' have been generated containing the results" + "\n" +
        "of this script")
else:
    print("File containing glance images 'quobyte_volumes.csv' is missing. Please create file of this name with quobyte volumes ids to test" + '\n' +
    "to continue")
